<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo;

use xibalba\mestizo\http\ServerRequest as Request;

use xibalba\mestizo\http\RequestAware;
use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;

use xibalba\ocelote\Checker;
use xibalba\ocelote\Inflector;
use xibalba\ocelote\ArrayHelper;

use \Exception;

/**
 * Resolve a url using an array of routes.
 *
 * A route can be static or dynamic. A static rule must match a URI, and must have defined a Controller
 * and an Action.
 * A Dynamic rule can compose of static parts and dynamic parts. The Controller, action and paarams are
 * infer matching regex.
 *
 * The routes must be config on an arrar like:
 *
 * [
 *   //Home rule
 *   '/' => [
 *      'controller' => 'ControllerClass',
 *      'action' => 'actionMethodName',
 *   ],
 *
 *   //Static rule
 *  '/static/content' => [
 *      'controller' => 'ControllerClass',
 *      'action' => 'actionMethodName',
 *   ]
 *
 *   //Static with namespaced controller
 *   '/static/namespace/content' => [
 *      'controller' => 'namespace/ControllerClass',
 *      'action' => 'actionMethodName',
 *   ]
 *
 *   // Dynamic namespace
 *   '/static/{namespace}/{controller}/{action}' => [
 *       'namespace' => ['key' => 'namespace', 'regex' => '(\w+)'],
 *       'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
 *       'action' => ['key' => 'action', 'regex' => '(\w+)'],
 *   ]
 *
 *   //Dynamic rule
 *  '/{controller}/{action}/{param1}/{param2}' => [
 *          'controller' => ['key' => 'controller', 'regex' => '(\w+)'],
 *          'action' => ['key' => 'action', 'regex' => '(\w+)'],
 *          'params' => [
 *              ['key' => 'param1',  'regex' => '(\w+)'],
 *              ['key' => 'param2',  'regex' => '(\w+)']
 *          ]
 *      ]
 *   //Combined rule
 *  '/post/{action}/{slug}' => [
 *      'controller' => 'Post',
 *      'action' => ['key' => 'action',  'regex' => '(\w+)'],
 *
 *      'params' => [
 *          ['key' => 'slug',  'regex' => '(\w+)']
 *      ]
 *  ]
 * ]
 *
 * A regex is any valid regular expresion.
 * «key» ever must match with the string between braces {}
 *
 * All the params matched are setted to $_params properties of request object.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.0
 */
class Router {
	use RequestAware;

	protected $_currentNamespace = null;
	protected $_currentControllerClass = null;
	protected $_currentAction = null;

	protected $_uri;

	/**
	 * Constructs a router.
	 *
	 * Try to infer the constroller, action and params from a URI.
	 *
	 * @param array $routes Routes array.
	 */
	public function __construct(array $routes) {
		if(!Checker::isAssociative($routes, true)) throw new Exception('Routes array malformed.');
		$this->_routes = $routes;
	}

	/**
	 * Returns the name of the controller class found.
	 * @return string Current controller class.
	 */
	public function getCurrentController() {
		if($this->_currentNamespace !== null) return $this->_currentNamespace.'\\'.$this->_currentControllerClass;
		return $this->_currentControllerClass;
	}

	/**
	 * Returns the name of the action method to call.
	 * @return string current action method.
	 */
	public function getCurrentAction() {
		return $this->_currentAction;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getRequest() {
		if($this->_request instanceof Request) return $this->_request;

		$request = Request::fromGlobals();
		$this->setRequest($request);

		return $this->_request;
	}

	/**
	 * Resolve the request uri.
	 * If the uri match with a rule, then set the controller class, action method and, if apply,
	 * the params for the request.
	 *
	 * If no uri match, an Exception is rised
	 */
	public function resolve() {
		$request = $this->getRequest();
		$basePath = '/';

		$this->_uri = $request->getUri();
		$realUrl = $this->_uri->getPath();

		foreach($this->_routes as $rule => $ruleDeff) {

			$this->_currentNamespace = null;
			$this->_currentControllerClass = null;
			$this->_currentAction = null;

			//All rules must define a controller and action
			if(!array_key_exists('controller', $ruleDeff) || !array_key_exists('action', $ruleDeff)) {
				throw new \Exception('A rule must be defined with a «controller» and «action» properties.');
			}

			if(array_key_exists('params', $ruleDeff) && !is_array($ruleDeff['params'])) {
				throw new \Exception('A rule with «params» defined must be as array.');
			}

			if(array_key_exists('methods', $ruleDeff)) {
				if(!in_array($request->getMethod(), $ruleDeff['methods'])) continue;
			}

			if($rule === '/') {
				//If the url is home, then just compare if match
				if($rule == $realUrl) {
					if(is_string($ruleDeff['controller']) && is_string($ruleDeff['action'])) {
						$this->_currentControllerClass = $ruleDeff['controller'];
						$this->_currentAction = $ruleDeff['action'];
					}
					else throw new \Exception('Must define a Controller and action for home rule.');

					break;
				}
			}
			else {
				/*
				 * If a uri and a rule are equal, then the rule is static.
				 * This rules does not have params.
				 */
				if($rule == $realUrl) {
					if(is_string($ruleDeff['controller']) && is_string($ruleDeff['action'])) {
						$this->_currentNamespace = ArrayHelper::getValue($ruleDeff, 'namespace');
						$this->_currentControllerClass = $ruleDeff['controller'];
						$this->_currentAction = $ruleDeff['action'];
					}
					else throw new \Exception('Must define a Controller and action for a static rule.');

					break;
				}
				else {
					//For evaluate a rule, the uri and rule must have the same count of parts divided by slash.
					$ruleParts = explode('/', trim(mb_substr($rule, 1)));
					$uriParts = explode('/', trim(mb_substr($realUrl, 1)));

					// Check for namespace. If exist and match, then settig up and remove part from fules and uri
					if(array_key_exists('namespace', $ruleDeff)) {
						$idxMatched = null;
						$namespaceDeff = $ruleDeff['namespace'];

						if(is_string($namespaceDeff)) $this->_currentNamespace = $namespaceDeff;
						else {
							foreach($uriParts as $idx => $part) {
								if($ruleParts[$idx] == '{'.$namespaceDeff['key'].'}'){
									if(preg_match($namespaceDeff['regex'], $part) == 1) {
										$this->_currentNamespace = $part;
										$idxMatched = $idx;
										break;
									}
								}
							}

							if($idxMatched !== null) {
								ArrayHelper::remove($ruleParts, $idxMatched);
								ArrayHelper::remove($uriParts, $idxMatched);
							}
						}
					}

					$cRuleParts = count($ruleParts);
					$cUriParts = count($uriParts);

					if($cRuleParts == $cUriParts) {
						/*
						 * The controller and action can apply for:
						 * 1- Are string than match with a controller class and action methods.
						 * 2- Or, are array with a key and regex definitions.
						 *
						 * All params must be defined as arrays.
						 */
						$controllerToken = '';
						$actionMatched = false;
						$params = [];

						//Eval each rule part
						for($i = 0; $i < $cRuleParts; $i++) {
							if(mb_substr($ruleParts[$i], 0, 1) == '{') {
								//If the first character is a brace, then eval for controller, action and params.

								//If current controller is null, eval
								if(is_null($this->_currentControllerClass)) {
									$controllerDeff = $ruleDeff['controller'];

									if(is_string($controllerDeff)) $this->_currentControllerClass = $controllerDeff;
									elseif($ruleParts[$i] == '{'.$controllerDeff['key'].'}') {
										if(preg_match($controllerDeff['regex'], $uriParts[$i]) == 1) $this->_currentControllerClass = Inflector::classify($uriParts[$i]);
									}
								}

								//If current action is null, eval
								if(is_null($this->_currentAction)) {
									$actionDeff = $ruleDeff['action'];
									if(is_string($actionDeff)) $this->_currentAction = $actionDeff;
									elseif($ruleParts[$i] == '{'.$actionDeff['key'].'}'){
										if(preg_match($actionDeff['regex'], $uriParts[$i]) == 1) $this->_currentAction = $uriParts[$i];
									}
								}

								//Eval for each param defined on the rule
								if(isset($ruleDeff['params'])) {
									foreach($ruleDeff['params'] as $paramRule){
										if($ruleParts[$i] == '{'.$paramRule['key'].'}'){
											if(preg_match($paramRule['regex'], $uriParts[$i]) == 1) $params[$paramRule['key']] = $uriParts[$i];
										}
									}
								}
							}
							else {
								//If is a static rule, the uri part and rule part must match
								if($ruleParts[$i] != $uriParts[$i]) break;
							}
						}//Rules part foreach end

						//For each rule, eval if has found controller, action, and if apply, params.
						if(!is_null($this->_currentControllerClass) && !is_null($this->_currentAction)) {
							if(isset($ruleDeff['params'])){
								if(count($ruleDeff['params']) == count($params)) {
									$this->setRequest($request->withQueryParams(ArrayHelper::merge($request->getQueryParams(), $params)));
									break;
								}
							}
							else break;
						}
					}//Fin del if cuando la cantidad de partes de la regla y la uri son iguales
				}//Fin del if para las reglas dinámicas
			}//Fin del else cuando la uri no es home "/"
		}//Fin del for que recorre las rutas configuradas.

		//If no action and controlle found, then rise Exception
		if(is_null($this->_currentControllerClass) || is_null($this->_currentAction)) throw new HttpException('No controller found for request', Http::NOT_FOUND);
	}//resolve() end
}
