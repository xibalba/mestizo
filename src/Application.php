<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo;

use xibalba\mestizo\controller\interfaces\BaseAbstract as IController;
use xibalba\mestizo\application\interfaces\BaseAbstract as IApplication;

use xibalba\mestizo\application\exception\Config as ConfigException;

use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;
use xibalba\mestizo\http\ResponseAware;

use xibalba\ocelote\traits\ConfigAware;
use xibalba\ocelote\Mime;

use xibalba\mestizo\http\interfaces\ServerRequest as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;

/**
 * Basic class for an Application.
 * This class setup the app config, init the application, catch the request and dispatch a response.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.1
 */
abstract class Application implements IApplication {
	use ResponseAware,
		ConfigAware {
			setConfig as baseSetConfig;
		}

	/**
	 * @type \xibalba\mestizo\Router
	 */
	protected $_router = null;

	/**
	 * Set the configuration for the App.
	 *
	 * @param array $config
	 * @throws \Exception
	 *
	 * @todo improve Exception and messages
	 */
	public function setConfig(array $config) {
		if(empty($config)) ConfigException::throwConfigEmpty();
		if(!isset($config['ns'])) ConfigException::throwNsEmpty();
		if(!isset($config['routes'])) ConfigException::throwRoutesEmpty();
		if(!isset($config['title'])) $config['app_title'] = 'Mestizo Application';

		$this::baseSetConfig($config);
	}

	/**
	 * Resove the request route for infer the required controller class, create a new instances
	 * of that controller and return it.
	 *
	 * @param Router|null $router
	 * @throws HttpException
	 * @return \xibalba\mestizo\controller\interfaces\BaseAbstract Controller instance.
	 */
	protected function resolveController() : IController {
		$router = $this->getRouter();

		$appNamespace = $this->_config['ns'].'\controllers\\';
		$controllerClass = $appNamespace . $router->getCurrentController();

		if(class_exists($controllerClass)) return new $controllerClass();
		throw new HttpException('Controller '.$controllerClass.' not found.', Http::NOT_FOUND);
	}

	/**
	 * Take a controller instance and distpatch an action by id.
	 * @param string $controller
	 * @param string $action
	 */
	protected function dispatchController(IController $controller, string $action) {
		//An action can return bodyResponse or a ResponseObject
		$bodyResponse = $controller->dispatchAction($action);

		if($bodyResponse instanceof IResponse) $this->sendResponse($bodyResponse);
		else{
			$response = $this->getResponse();
			if(!empty($bodyResponse)) $response = $response->withBody(Psr7\stream_for($bodyResponse));

			$this->sendResponse($response);
		}
	}

	/**
	 * Return the request object.
	 *
	 * @return \xibalba\mestizo\http\interfaces\ServerRequest
	 */
	public function getRequest() : IRequest {
		return $this->getRouter()->getRequest();
	}

	/**
	 * Set the request object.
	 *
	 * @param \xibalba\mestizo\http\interfaces\ServerRequest
	 * @return $this
	 */
	public function setRequest(IRequest $request) {
		$this->getRouter()->setRequest($request);
		return $this;
	}

	/**
	 * Return the Router instance.
	 * This method also work as a Factory if there is no set an Router instance.
	 * @return Router Router instance
	 */
	protected function getRouter() : Router {
		if($this->_router === null) $this->_router = new Router($this->getConfig('routes'));
		return $this->_router;
	}

	/**
	 * Execute the application for a {Request -> Process -> Responce} flow.
	 * @throws \Exception
	 */
	public function run() {
		if(!$this->hasConfig()) throw new ConfigException('Cannot run application witout configuration.');

		$router = $this->getRouter();

		try {
			$router->resolve();
			$this->dispatchController($this->resolveController(), $router->getCurrentAction());
		}
		catch(HttpException $e) {
			$response = $this->getResponse();
			$response = $response->withStatus($e->getCode(), $e->getMessage());

			//If a HttpException is catched, send a respone with Exception info.
			if($this->getRequest()->getHeaderLine("Accept") === Mime::getTypeByExt('json')) {
				$errorData = [
					'code' => $e->getCode(),
					'message' => $e->getMessage()
				];

				$response = new Response(
					$errorData['code'],
					['Content-Type' => Mime::getTypeByExt('json')],
					json_encode($errorData),
					'1.1',
					$errorData['message']
				);
			}

			$this->sendResponse($response);
		}
	}

	protected function sendResponse(IResponse $response) {
		if (headers_sent()) return;

		$status = $response->getStatusCode();

		header(sprintf('HTTP/%s %s %s', $response->getProtocolVersion(), $status, $response->getReasonPhrase()), true, $status);

		foreach ($response->getHeaders() as $name => $values) {
			foreach ($values as $value) {
				header(sprintf('%s: %s', $name, $value), false);
			}
		}

		echo $response->getBody()->getContents();

		if(function_exists('fastcgi_finish_request')) fastcgi_finish_request();
	}

	/**
	 * Return the configured base domain, or the request base url.
	 */
	public function getBaseDomain() {
		if(is_string($this->getConfig('base_domain'))) return $this->getConfig('base_domain');
		return $this->getRequest()->getUrl();
	}
}
