<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\controller;

use Psr\Http\Message\ResponseInterface as IResponse;

use xibalba\mestizo\Application as App;
use xibalba\mestizo\http\interfaces\ServerRequest as IRequest;
use xibalba\mestizo\controller\interfaces\BaseAbstract as IController;
use xibalba\mestizo\controller\interfaces\ActionFilter as IActionFilter;

use xibalba\mestizo\http\RequestAware;
use xibalba\mestizo\http\Exception as HttpException;
use xibalba\mestizo\http\Http;

use xibalba\ocelote\Inflector;

/**
 * A controller has the responsibility for process the requests and generate responses.
 * The logic is encapsulated on multiple actions. Each action is a public method as part of
 * controller classes or traits.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.0
 */
abstract class BaseAbstract implements IController, IActionFilter {
	use RequestAware;

	const ACTION_PREFIX = '';
	const ACTION_SUFIX = '';

	/**
	 * @var ActionFilter[] All Action filters to be executed before the action.
	 */
	protected $_actionFilters = [];

	/**
	 * Construct a controller
	 *
	 * @param Request $request. By default the app request object.
	 */
	public function __construct(IRequest $request = null) {
		if(is_null($request)) $request = $this->getAppInstance()->getRequest();
		$this->setRequest($request);
		$this->init();
	}

	protected function init() {}

	/**
	 * @inheritdoc
	 */
	public function afterAction(string $action) {}

	/**
	 * This method is invoked before the action execution.
	 *
	 * This method will call `beforeAction()` method of each ActionFilter configured
	 * to the controller.
	 *
	 * If you override this method, your code must look like:
	 * ~~~
	 * public function beforeAction($action){
	 *     if(parent::beforeAction($action)){
	 *         //Your filter logic.
	 *         return true; //or false.
	 *     }
	 *     else return false;
	 * }
	 * ~~~
	 *
	 * @param string $action The action name to be executed.
	 * @return boolean whether filter applied.
	 */
	public function beforeAction(string $action) : bool {
		foreach($this->_actionFilters as $filter) {
			if($filter->beforeAction($action) === false) return false;
		}
		return true;
	}

	/**
	 * Add an ActionFilter to the controller.
	 *
	 * ~~~
	 * protected function init(){
	 *     $actionFilter = new ActionFilterClass();
	 *     $this->addActionFilter($actionFilter);
	 * }
	 * ~~~
	 *
	 * @param IActionFilter action filter object.
	 */
	protected function addActionFilter(IActionFilter $filter) {
		$this->_actionFilters[] = $filter;
	}

	/**
	 * This method try to execute a controller action method.
	 *
	 * When is used the mestizo Application class, the run() method
	 * invoke this method.
	 *
	 * @param string $action the action name (without prefix or sufix)
	 * @return Psr\Http\Message\ResponseInterface
	 */
	public function dispatchAction(string $action) : IResponse {
		$bodyResponse = null;

		$actionMethodName = static::ACTION_PREFIX.$action.static::ACTION_SUFIX;

		//Parse action to camelCase
		$actionMethodName = lcfirst(Inflector::camelize($actionMethodName));

		if(method_exists($this, $actionMethodName)) {
			if($this->beforeAction($action)) {
				$bodyResponse = $this->$actionMethodName();
				$this->afterAction($action);
			}
		}
		else throw new HttpException('Not found', Http::NOT_FOUND);

		if($bodyResponse instanceof IResponse) return $bodyResponse;
		else {
			$response = $this->getResponse();
			$response = $response->withBody($bodyResponse);

			return $response;
		}
	}
}
