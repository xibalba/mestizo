<?php
/**
 * @copyright	2014-2015 Xibalba Lab.
 * @license     http://opensource.org/licenses/bsd-license.php
 * @link		https://bitbucket.org/xibalba/mestizo
 */

namespace xibalba\mestizo\controller;

use xibalba\mestizo\Application as App;
use xibalba\mestizo\controller\interfaces\ActionFilter;

use xibalba\mestizo\http\traits\RequestAware;
use xibalba\mestizo\http\Request;

/**
 * AccessManager provides a simple way for access control bases on rules.
 *
 * This is an action filter. It will check its [[rules]] to find
 * the first rule that matches the current context variables (request method, IP, user role).
 * The matching rule will dictate whether to allow or deny the access to requested controller action.
 * If no rule matches, the access will be denied.
 *
 * To use AccessControll, override the `__constuct()` method of your controller class for atach an
 * AccessManager instance to the actionFilter.
 *
 * ~~~
 * public function __contruct($request = null){
 *     parent::__construct($request);
 *
 *     $aM = new AccessManager();
 *     $aM->addRule([
 *         'allow'   => true, // Or false
 *         'actions' => ['login', 'signup'], // Do not set for apply to all actions
 *         'verbs'   => ['GET', 'POST'], // Or the rest of request methods
 *         'ips'      => ['12.7.0.1'], // Add many ip's as ypu need.
 *         'user'    => '?' // Or '@' for authenticated users.
 *     ]);
 *
 *     $this->addActionFilter($aM);
 * }
 * ~~~
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 0.1
 */
class AccessManager implements ActionFilter{
	use RequestAware;
	
	protected $_rules = [];
	
	public function __construct($request = null){
		if(is_null($request)) $request = App::getInstance()->getRequest();
		$this->setRequest($request);
	}
	
	public function beforeAction($action){
		$user = '?';
		
		foreach($this->_rules as $rule){
			if($this->applyRuleToAction($rule['actions'], $action)){
				if($this->matchRule($rule, $action, $user)) return $rule['allow'] ? true : false;
				else return false;
			}
		}
		
		return true;
	}
	
	protected function applyRuleToAction($ruleActions, $action){
		if($ruleActions == '*') return true;
		else return in_array($action, $ruleActions, true) ? true : false;
	}
	
	protected function matchRule($rule, $action, $user){
		if(isset($rule['verbs'])){
			$verb = $this->getRequest()->getMethod();
			if(!in_array($verb, $rule['verbs'], true)) return false;
		}
		
		if(isset($rule['ips'])){
			$requestIp = $this->getRequest()->getClientIp();
			if(!in_array($requestIp, $rule['ips'], true)) return false;
		}
		
		//@todo match user role
		/*if(isset($rule['user'])){
			
		}//*/
		
		return true;
	}
	
	public function addRule($rule){
		if(!in_array($rule, $this->_rules, true)) $this->_rules[] = $rule;
		else throw new \Exception('This rule was added before. Check your code.');
	}
}