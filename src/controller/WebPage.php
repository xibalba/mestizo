<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\controller;

use xibalba\mestizo\http\ResponseAware;
use xibalba\mestizo\http\responder\HtmlRenderer;

use GuzzleHttp\Psr7\ServerRequest as Request;

/**
 * This class is a base controller for traditional web page responses.
 * Try to set:
 *
 * * A basic configuration for use the HtmlRenderer trait.
 * * ViewPath from «`base_views_path`» application configuration.
 * * Layout name, «`main`» by default.
 *
 * @since 0.1
 * @version 0.3
 */
abstract class WebPage extends BaseAbstract {
	use HtmlRenderer, ResponseAware;

	/**
	 * @inheritdoc
	 */
	public function __construct(Request $request = null) {
		parent::__construct($request);
		$this->setResponse($this->getAppInstance()->getResponse());

		$this->_layout = 'main';
		$this->setViewPath($this->getAppInstance()->getConfig('base_views_path'));
	}

	/**
	 * Shorcut for «`$this->getResponse()->redirect()`» or
	 * «`Application->getResponse()->redirect()`».
	 *
	 * This method return a Response object setted for redirect to the passed destination,
	 * tacking the configured base domain and concatenating the destination.
	 *
	 * @param string $destination Destination, i.e. 'controller/action'.
	 *
	 * @return Response Response to redirect.
	 */
	protected function redirect($destination){
		return $this->getResponse()->redirect($this->getAppInstance()->getBaseDomain() . $destination);
	}
}
