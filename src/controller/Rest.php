<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\controller;

use xibalba\mestizo\http\Http;
use xibalba\mestizo\http\RequestAware;
use xibalba\mestizo\http\ResponseAware;

use xibalba\mestizo\http\Exception as HttpException;

use xibalba\ocelote\Mime;
use xibalba\ocelote\Inflector;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use Psr\Http\Message\ResponseInterface as IResponse;

/**
 * A controller has the responsibility for process the requests and generate responses.
 * The logic is encapsulated on multiple actions. Each action is a public method as part of
 * controller classes or traits.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 0.4
 */
abstract class Rest extends BaseAbstract {
	use ResponseAware;

	/**
	 * {@inheritdoc}
	 */
	public function dispatchAction(string $action) : IResponse {
		$request = $this->getRequest();
		$bodyResponse = null;

		$httpMethod = strtolower($request->getMethod());

		$actionMethodName = static::ACTION_PREFIX.$httpMethod.$action.static::ACTION_SUFIX;
		$actionMethodName = lcfirst(Inflector::camelize($actionMethodName));

		if(method_exists($this, $actionMethodName)) {
			if($this->beforeAction($action)) {
				$bodyResponse = $this->$actionMethodName();
				$this->afterAction($action);
			}
		}
		else throw new HttpException('Resource not found', Http::NOT_FOUND);

		if($bodyResponse instanceof IResponse) return $bodyResponse;
		else {
			$response = $this->getResponse();
			$response = $response->withBody($bodyResponse);
			$response->withHeader('Content-Type', Mime::getTypeByExt('json'));

			return $response;
		}
	}
}
