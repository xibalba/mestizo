<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\controller\interfaces;

use xibalba\mestizo\application\interfaces\BaseAbstract as IAppication;

/**
 * This interface expose how must be defined as minimun a controller
 * for to be allowed to used by a Mestizo application.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface BaseAbstract {
	/**
	 * This method is invoked after the acction execution.
	 *
	 * @param string $action The action name.
	 */
	public function afterAction(string $action);

	/**
	 * This method is invoked before the action execution.
	 * If this method return a false value then must be avoided the
	 * action execution.
	 *
	 * @param string $action The action name to be executed.
	 * @return boolean whether if the action can execute or not.
	 */
	public function beforeAction(string $action) : bool;
	
	/**
	 * This method try to execute a controller action method.
	 * This method must return an Response instance or a string.
	 *
	 * @param string $action the action name.
	 * @return Psr\Http\Message\ResponseInterface | string
	 */
	public function dispatchAction(string $action);

	/**
	 * This method must return the Application Instance. Since each
	 * application have it's own implementation, by this way ensure to
	 * of return the correct application instance.
	 * 
	 * @see Application
	 */
	public function getAppInstance() : IAppication;
}