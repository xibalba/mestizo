<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\controller\interfaces;

/**
 * This interface must be used by any controller that require filter
 * before the execution of some action.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface ActionFilter {
	/**
	 * Execute some validation for filter if an action can be executed or not.
	 *
	 * @param string $actionId
	 * @return bool
	 */
	public function beforeAction(string $actionId) : bool;
}