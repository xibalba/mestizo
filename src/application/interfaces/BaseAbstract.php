<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\application\interfaces;

use xibalba\ocelote\interfaces\Configurable;

use xibalba\mestizo\http\interfaces\Requestable;
use xibalba\mestizo\http\interfaces\Responseable;

/**
 * This interface expose how must be defined as minimun an application class.
 * The basic design is based on singleton pattern with a router that resolve
 * the request and return to the client a response.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface BaseAbstract extends Configurable, Requestable, Responseable {
	/**
	 * Return singleton application object.
	 *
	 * @return \xibalba\mestizo\Application singleton application object.
	 */
	public static function getInstance() : BaseAbstract;

	/**
	 * Execute the application for a {Request -> Process -> Responce} flow.
	 * @throws \Exception
	 */
	public function run();
}