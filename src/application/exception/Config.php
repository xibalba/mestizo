<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\application\exception;

class Config extends \Exception {
	const CONFIG_EMPTY = [
		'code' => 0x00,
		'msg' => 'A Mestizo application require a configuration array.'
	];

	const NS_EMPTY = [
		'code' => 0x01,
		'msg' => 'A Mestizo application require a namespace setted on "ns" property.'
	];

	const ROUTES_EMPTY = [
		'code' => 0x02,
		'msg' => 'A Mestizo application require a routes configuration on "routes" property.'
	];

	public static function throwConfigEmpty() {
		throw new self(self::CONFIG_EMPTY['msg'], self::CONFIG_EMPTY['code']);
	}

	public static function throwNsEmpty() {
		throw new self(self::NS_EMPTY['msg'], self::NS_EMPTY['code']);
	}

	public static function throwRoutesEmpty() {
		throw new self(self::ROUTES_EMPTY['msg'], self::ROUTES_EMPTY['code']);
	}
}