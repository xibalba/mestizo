<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\application\traits;

use xibalba\mestizo\application\interfaces\BaseAbstract as IApplication;

/**
 * Provide a Singleton pattern to Application classes.
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait SingletonAware {
	/**
	 * @type \xibalba\mestizo\Application
	 */
	protected static $_instance = null;

	/**
	 * Construct an Application object.
	 * Checks for minimun configuration elements.
	 *
	 * @param array $config
	 * @throws \Exception
	 */
	protected function __construct() {}

	// Do not allow to clone
	protected function __clone() {}

	// Do not allow to wakeup
	protected function __wakeup() {}

	/**
	 * Return singleton application object.
	 *
	 * @return \xibalba\mestizo\Application singleton application object.
	 */
	public static function getInstance() : IApplication {
		if(static::$_instance === null) static::$_instance = new static();
		return static::$_instance;
	}
}
