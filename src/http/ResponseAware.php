<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http;

use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;

/**
 * Permits a class to interact with a response object.
 *
 * @package xibalba\mestizo\http\traits;
 */
trait ResponseAware {
	/**
	 * Request object.
	 *
	 * @type \Psr\Http\Message\ResponseInterface
	 */
	protected $_response;

	/**
	 * Return the request object.
	 *
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function getResponse() : IResponse {
		if($this->_response instanceof IResponse) return $this->_response;
		return $this->_response = new Response();
	}

	/**
	 * Set the response object.
	 *
	 * @param \Psr\Http\Message\ResponseInterface $response
	 * @return $this
	 */
	public function setResponse(IResponse $response) {
		$this->_response = $response;
		return $this;
	}
}