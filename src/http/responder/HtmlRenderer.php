<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

 namespace xibalba\mestizo\http\responder;

use xibalba\mestizo\http\Http;
use GuzzleHttp\Psr7\Response;

/**
 * Provide functionality for render html from php views
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.1
 */
trait HtmlRenderer {
	protected $_layout = '';
	protected $_layoutDirName = 'layouts';
	protected $_viewPath = '';
	protected $_viewDirName = '';

	/**
	 * Set the viewpath for template views
	 *
	 * @param string $path Path of the views
	 */
	protected function setViewPath($path) {
		$this->_viewPath = $path;
	}

	/**
	 * Set the directory name for the layout
	 *
	 * @param string $dirName the layout directory name to set.
	 */
	protected function setLayoutDirName($dirName) {
		$this->_layoutDirName = $dirName;
	}

	/**
	 * Set the directory name for view.
	 *
	 * @param string $dirName the view directory name to set.
	 */
	protected function setViewDirname($dirName) {
		$this->_viewDirName = $dirName;
	}

	/**
	 * Returns the layout file name.
	 *
	 * @throws Exception if no layout has set,
	 * @return string The layout file name.
	 */
	protected function getLayoutFileName() {
		if(empty($this->_layout)) throw new \Exception('No layout is set');
		return $this->_layout.'.php';
	}

	/**
	 * Returns the directory name for search the layout file.
	 *
	 * @throws Exception if no layout directory name has set.
	 * @return string The firectory name for search the layout file.
	 */
	protected function getLayoutDirName() {
		if(empty($this->_layoutDirName)) throw new \Exception('No layout directory name is set.');
		return $this->_layoutDirName;
	}

	/**
	 * Return a processed html from the passed view name.
	 *
	 * @param string $view name of the template to use.
	 * @param array $params Params for pass to the view template.
	 *
	 * @return string html result.
	 */
	protected function getTemplate($view, $params = []) {
		$viewFile = $this->getViewPath().$this->getViewDirName().'/'.$view.'.php';
		return $this->renderFile($viewFile, $params);
	}

	/**
	 * Returns the base view path for search view files.
	 *
	 * @throws Exception if no view path has set.
	 * @return string
	 */
	protected function getViewPath() {
		if(empty($this->_viewPath)) throw new \Exception('No view path is set.');
		return $this->_viewPath;
	}

	protected function getViewDirName() {
		if(empty($this->_viewDirName)) return '';
		return '/'.$this->_viewDirName;
	}

	/**
	 * Compose a html document with the passed view tenplate and the configured layout
	 * and return that.
	 *
	 * @param string $view Name of the view to render
	 * @param array $params Params for pass to the view
	 */
	public function render($view, $params = []) {
		$layoutFile = $this->getViewPath().'/'.$this->getViewDirName().'/'.$this->getLayoutDirName().'/'.$this->getLayoutFileName();
		$params['content'] = $this->getTemplate($view, $params);

		$bodyResponse = $this->renderFile($layoutFile, $params);
		$r = new Response(200, [], $bodyResponse);
		return new Response(Http::OK, [], $bodyResponse);
	}

	/**
	 * Return a html from the passed view file.
	 *
	 * @param string $viewFile Full route and file name of the view to use.
	 * @param array $params Params for pass to the view.
	 *
	 * @throws Exception if view file cannot be found.
	 *
	 * return string html result.
	 */
	public function renderFile($viewFile, $params = []) {
		if(!file_exists($viewFile)) throw new \Exception(sprintf('Template %s file does not exist.', $viewFile));

		ob_start();
		if(!empty($params)) extract($params);
		require($viewFile);

		return ob_get_clean();
	}
}
