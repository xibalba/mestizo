<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\responder;

use xibalba\mestizo\Application as App;
use xibalba\mestizo\http\Http;

use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;

use xibalba\ocelote\Mime;

/**
 * Provide functionality for respond as a Json Respond.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait Json {
	/**
	 * Return a Response object where the body is setted as a JSON
	 * of $data parameter passed.
	 *
	 * @param array $data Data to be coded as JSON.
	 * @return IResponse The response
	 */
	public function respond(array $data, $statusCode = Http::OK) : IResponse {
		return new Response($statusCode, ['Content-Type' => Mime::getTypeByExt('json')], json_encode($data));
	}

	/**
	 * Shorthand method for return a response as Created (201).
	 * 
	 * @param array $data Data to be coded as JSON.
	 * @return IResponse The response
	 */
	public function respondCreated(array $data) : IResponse {
		return $this->respond($data, Http::CREATED);
	}

	/**
	 * Shorthand method for return a response as No Content (204).
	 *
	 * @return IResponse The response
	 */
	public function respondNoContent() : IResponse {
		return $this->respond([], Http::NO_CONTENT);
	}
}