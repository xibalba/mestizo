<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\responder;

use xibalba\mestizo\http\Http;

use Psr\Http\Message\ResponseInterface as IResponse;
use GuzzleHttp\Psr7\Response;

/**
 * Provide a basic functionality for respond text.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait BasicAware {
	/**
	 * Return a Response object.
	 *
	 * @param string $body Body string for the response.
	 * @param int $statusCode Http Status Code for the response.
	 *
	 * @return Response The response.
	 */
	public function respond(string $body, int $statusCode = Http::OK) : IResponse {
		return new Response($statusCode, [], $body);
	}

	/**
	 * Return a Response object with an empty body.
	 *
	 * @param int $statusCode Http Status Code for the response.
	 * @return Response The response.
	 */
	public function respondEmpty(int $statusCode = Http::NO_CONTENT) : IResponse {
		return new Response($statusCode);
	}
}