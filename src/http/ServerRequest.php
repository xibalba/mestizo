<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http;

use Psr\Http\Message\StreamInterface as IStream;

use GuzzleHttp\Psr7\ServerRequest as BaseAbstract;
use GuzzleHttp\Psr7\LazyOpenStream;

use xibalba\mestizo\http\interfaces\ServerRequest as ServerRequestInterface;

use xibalba\ocelote\ArrayHelper;
use xibalba\ocelote\Converter;

/**
 * {@inheritdoc}
 */
class ServerRequest extends BaseAbstract implements ServerRequestInterface {
	/**
	 * {@inheritdoc}
	 */
	public static function fromGlobals() {
		$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
		$headers = getallheaders();
		$uri = self::getUriFromGlobals();
		$body = new LazyOpenStream('php://input', 'r+');
		$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? str_replace('HTTP/', '', $_SERVER['SERVER_PROTOCOL']) : '1.1';
		$parsedBody = ($method == 'POST') ? $_POST : Converter::queryStringToArray((string) $body);

		$serverRequest = new static($method, $uri, $headers, $body, $protocol, $_SERVER);

		return $serverRequest
			->withCookieParams($_COOKIE)
			->withQueryParams($_GET)
			->withParsedBody($parsedBody)
			->withUploadedFiles(self::normalizeFiles($_FILES));
	}
	
	/**
	 * Retrive a query string argument. If $key does not exist as
	 * part of the query string then a `$default` value can be returned.
	 *
	 * @param string $key Key name of query string
	 * @param mixed $default A default for for return if `$key` does not exist
	 * on query string. `null` by default.
	 */
	public function getQueryParam(string $key, $default = null) {
		return ArrayHelper::getValue($this->getQueryParams(), $key, $default);
	}

	/**
	 * Retrive a server parameter.
	 */
	public function getServerParam(string $key, $default = null) {
		return ArrayHelper::getValue($this->getServerParams(), $key, $default);
	}

	/**
	 * Retrive a parsed body parameter.
	 */
	public function getBodyParam(string $key, $default = null) {
		return ArrayHelper::getValue($this->getParsedBody(), $key, $default);
	}

	/**
	 * Get the IP address of the client, the correct way.
	 *
	 * @return string
	 */
	public function getClientIP() : string {
		$headers = ['HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR', 'HTTP_CLIENT_IP'];

		foreach ($headers as $header) {
			if($ip = $this->getServerParam($header)) {
				if (strpos($ip, ',') !== false) $ip = trim(explode(',', $ip)[0]);
				return $ip;
			}
		}

		return '';
	}
}