<?php
/**
 * @copyright	2014-2015 Xibalba Lab.
 * @license     http://opensource.org/licenses/bsd-license.php
 * @link		https://bitbucket.org/xibalba/mestizo
 */

namespace xibalba\mestizo\http;

use \Exception as PhpException;
use xibalba\mestizo\http\Http;

/**
 * Exception class for handle http exceptions.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Exception extends PhpException {
	
	/**
	 * {@inheritdoc}
	 */
	public function __construct($message = null, $code = 0, \Exception $previous = null){
		if(!$message){
			try{
				$message = Http::getStatusCode($code);
			}
			catch(\Exception $e){
				$code = 0;
			}
		}
		parent::__construct($message, $code, $previous);
	}
}