<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http;

use xibalba\mestizo\http\interfaces\ServerRequest as IRequest;

/**
 * Provide to a class to interact with a request object.
 *
 * @package xibalba\mestizo\http\traits;
 * @author E. Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait RequestAware {
	/**
	 * Request object.
	 *
	 * @type \xibalba\mestizo\http\ServerRequest
	 */
	protected $_request;

	/**
	 * Return the request object.
	 *
	 * @return \xibalba\mestizo\http\interfaces\ServerRequest
	 */
	public function getRequest() : IRequest {
		return $this->_request;
	}

	/**
	 * Set the request object.
	 *
	 * @param \xibalba\mestizo\http\interfaces\ServerRequest
	 * @return $this
	 */
	public function setRequest(IRequest $request) {
		$this->_request = $request;
		return $this;
	}
}