<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\interfaces;

use xibalba\mestizo\http\interfaces\ServerRequest as IRequest;

/**
 * This interface expose methods for basic Request manipulation.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface Requestable {
	/**
	 * Return the request object.
	 *
	 * @return \xibalba\mestizo\http\interfaces\ServerRequest
	 */
	public function getRequest() : IRequest;

	/**
	 * Set the request object.
	 *
	 * @param \xibalba\mestizo\http\interfaces\ServerRequest
	 */
	public function setRequest(IRequest $request);
}
