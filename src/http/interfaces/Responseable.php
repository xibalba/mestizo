<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\interfaces;

use Psr\Http\Message\ResponseInterface as IResponse;

/**
 * This interface expose methods for basic Response manipulation.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface Responseable {
	/**
	 * Return the response object.
	 *
	 * @return \xibalba\mestizo\http\interfaces\ServerRequest
	 */
	public function getResponse() : IResponse;

	/**
	 * Set the response object.
	 *
	 * @param \Psr\Http\Message\ResponseInterface $response
	 */
	public function setResponse(IResponse $response);
}
