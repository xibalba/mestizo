<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\interfaces;

use Psr\Http\Message\ServerRequestInterface;

interface ServerRequest extends ServerRequestInterface {
	/**
	 * Retrive a query string argument. If $key does not exist as
	 * part of the query string then a `$default` value can be returned.
	 *
	 * @param string $key Key name of query string
	 * @param mixed $default A default for for return if `$key` does not exist
	 * on query string. `null` by default.
	 */
	public function getQueryParam(string $key, $default = null);

	/**
	 * Retrive a server parameter.
	 *
	 * @param string $key Key name of query string
	 * @param mixed $default A default for for return if `$key` does not exist
	 * on query string. `null` by default.
	 */
	public function getServerParam(string $key, $default = null);

	/**
	 * Retrive a parsed body parameter.
	 *
	 * @param string $key Key name of query string
	 * @param mixed $default A default for for return if `$key` does not exist
	 * on query string. `null` by default.
	 */
	public function getBodyParam(string $key, $default = null);

	/**
	 * Get the IP address of the client, the correct way.
	 *
	 * @return string
	 */
	public function getClientIP() : string;
}
