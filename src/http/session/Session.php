<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\session;

use xibalba\ocelote\traits\Mutable;

/**
 * This class wraps the http session behavior.
 * Provide the methods for start, destroy and check
 * sessions.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Session {
	use Mutable;

	/**
	* Provide if the session is started or not.
	* @type boolean
	*/
	protected $_started = false;
	
	protected $_handler = null;

	/**
	 * Build the session object.
	 * Set as data value a reference to $_SESSION superglobal.
	 *
	 * @throws \Exception
	 */
	public function __construct(\SessionHandlerInterface $handler = null) {
		if($handler !== null) $this->setHandler($handler);
		if(!$this->start()) throw new \Exception('Cannot start session. Headers has been sent.');
		$this->_data = &$_SESSION;
	}

	/**
	 * Start a session when is possible.
	 * @return boolean true if session was started, otherwise false.
	 */
	public function start() {
		if(!headers_sent()) {
			if(session_start()) return $this->_started = true;
			return false;
		}
		return false;
	}

	/**
	 * Destroy a session and clean session vars.
	 */
	public function destroy() {
		session_unset();
		session_destroy();
		$this->_started = false;
	}

	/**
	 * Returns the current session ID. regenerate one.
	 * @return string current session id. An empty string is returned if
	 * no id exist.
	 */
	public function getId() {
		return session_id();
	}

	/**
	 * Regenerate the current session and apply a new session ID.
	 *
	 * @param bool $delete
	 * @return bool
	 */
	public function regenerateId($delete = true) {
		return session_regenerate_id($delete);
	}

	/**
	 * @return boolean whether the session has started
	 */
	public function isActive() {
		return session_status() == PHP_SESSION_ACTIVE;
	}

	/**
	 * @return boolean true for a started session, otherwise false.
	 */
	public function isStarted() {
		return $this->_started;
	}

	public function setHandler(\SessionHandlerInterface $handler) {
		$this->_handler = $handler;
		session_set_save_handler($this->_handler);
	}

	public function getHandler() : \SessionHandlerInterface {
		return $this->_handler;
	}
}
