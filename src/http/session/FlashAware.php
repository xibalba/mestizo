<?php
/**
 * @copyright	2014-2015 Xibalba Lab.
 * @license     http://opensource.org/licenses/bsd-license.php
 * @link		https://bitbucket.org/xibalba/mestizo
 */

namespace xibalba\mestizo\http\session\traits;

use xibalba\mestizo\Application as App;

/**
 * This trait add funtionality for work with flashes.
 * A flash is a session key-value pair thath exist until is getted.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 0.1
 */
trait FlashAware {
	public function setFlash($key, $value){
		App::getInstance()->getSession()->set($key, $value);
	}
	
	public function hasFlash($key){
		return App::getInstance()->getSession()->has($key);
	}
	
	public function getFlash($key){
		$session = App::getInstance()->getSession();
		
		$value = $session->get($key);
		if($value !== null) $session->remove($key);
		
		return $value;
	}
	
	public function removeFlash($key){
		$session = App::getInstance()->getSession();
		if($session->has($key)) $session->remove($key);
	}
}