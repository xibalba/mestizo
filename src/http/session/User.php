<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\session;

use xibalba\mestizo\user\BaseAbstract;

/**
 * User is the class for a "session web user" abstraction.
 *
 * Basically you must use this class to identify if a user
 * is a guest or a logged user. After that, you must implement
 * you own aunthentication process.
 *
 * For store user related variables, use:
 * ~~~
 * $session = Application::getSession();
 * $session->set('key', $var);
 * $var = $session->get('key');
 * ~~~
 *
 * For check id the user is logged:
 * ~~~
 * public function login(){
 *     $user = App::getInstance()->getUser();
 *     if($user->isLogged()){
 *         $time = 30; // Or any duration
 *         $user->login($id, $time);
 *     }
 *     //Redirect
 * }
 * ~~~
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.0
 */
abstract class User extends BaseAbstract {
	use Aware;

	/**
	 * @const SESSION_ID_PARAM is used for define the cookie id param for retrive
	 * a user session.
	 */
	const SESSION_ID_PARAM = '_uid';

	public function __construct() {
		$session = $this->getSession();

		if($session->isStarted() && $session->has(static::SESSION_ID_PARAM)) {
			parent::__construct($session->get(static::SESSION_ID_PARAM));
		}
		else parent::__construct();
	}

	/**
	 * @inhericdoc
	 */
	public function login($id) : bool {
		$session = $this->getSession();
		
		if(!$session->isStarted()) {
			if(!$session->start()) return false;
		}
		else $session->regenerateId();

		$session->remove(static::SESSION_ID_PARAM);
		$session->set(static::SESSION_ID_PARAM, $id);

		return parent::login($id);
	}

	/**
	 * @inhericdoc
	 */
	public function logout() {
		$this->getSession()->destroy();
		parent::logout();
	}
}