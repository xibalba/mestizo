<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\session;

use xibalba\mestizo\Application as App;
use xibalba\ocelote\Checker;

use xibalba\tuza\DbConnectable;
use xibalba\tuza\DbAware;

/**
 * DbSessionHandler allow to store session data at database instead of disk files.
 */
class DbHandler implements \SessionHandlerInterface, DbConnectable {
	use DbAware;

	protected static $_tableName = null;

	protected $_exist = false;

	public function __construct() {
		if(static::$_tableName === null) static::$_tableName = 'mz_sessions';
		static::setDbConnection(App::getInstance()::getDbConnection());
	}

	public static function getTableName() {
		return static::$_tableName;
	}

	/**
	 * {@inheritDoc}
	 */
	public function close() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public function destroy($id) {
		static::getDbConnection()->delete(static::$_tableName, ['id' => $id]);
		$this->_exist = false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public function gc($maxLifeTime) {
		static::getDbConnection()->delete(static::$_tableName, ['last_activity[<=]' => (time() - $maxLifeTime)]);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public function open($savePath, $name) {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public function read($id) {
		$sessData = static::getDbConnection()->selectScalar(static::$_tableName, ['payload'], ['id' => $id]);
		if($sessData === false) return '';
		else {
			$this->_exist = true;
			return base64_decode($sessData['payload']);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function write($id, $payload) {
		if (!Checker::isEmpty($payload)) {
			$db = static::getDbConnection();
			$data = [
				'payload' => base64_encode($payload),
				'last_activity' => time()
			];

			if($this->_exist) {
				if($db->update(static::$_tableName, $data, ['id' => $id]) == 1) return true;
				return false;
			}
			else {
				$data['id'] = $id;
				if($db->insert(static::$_tableName, $data) == $id) return $this->_exist = true;
				return false;
			}
		}
	}
}