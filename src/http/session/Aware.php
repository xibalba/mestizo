<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\http\session;

use xibalba\mestizo\Application as App;
use xibalba\mestizo\application\exception\Config as ConfigException;

use xibalba\ocelote\ArrayHelper;

/**
 * This trait provide to classes the accesor and propertie needed for a basic Session handling.
 * Any class that include this trait must be an Configurable class.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\ocelote\traits
 */
trait Aware {
	protected $_session = null;

	/**
	 * Set the Session object.
	 * @param \xibalba\mestizo\http\session\Session $session
	 */
	public function setSession(Session $session) {
		$this->_session = $session;
	}

	/**
	 * @return \xibalba\mestizo\http\session\Session
	 */
	public function getSession() : Session {
		if($this->_session instanceof Session) return $this->_session;

		$sessionConfig = $this->getAppInstance()->getConfig('session', []);

		$sessionClass = $sessionConfig['class'] ?? Session::class;
		$handlerClass = $sessionConfig['handler_class'] ?? null;
		$handler = null;
		
		if($handlerClass === 'Db') $handlerClass = DbHandler::class;
		if(!empty($handlerClass)) $handler = new $handlerClass();

		$this->setSession(new $sessionClass($handler));
		return $this->_session;
	}
}