<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\user\interfaces;

/**
 * This interface must be used for any class that will to abstract the user for
 * status check purporse.
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.0
 */
interface BaseAbstract {
	/**
	 * Returns the user id value.
	 * @return mixed the user id value.
	 */
	public function getId();

	/**
	 * Returns the value wheter the user is aunthenticated or not.
	 * @return boolean whether the user is a guest (non authenticated)
	 */
	public function isLogged(): bool;

	/**
	 * Register the user id and start a session.
	 */
	public function login($id): bool;

	/**
	 * Destroys the user session.
	 */
	public function logout();
}