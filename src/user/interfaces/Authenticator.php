<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\user\interfaces;

/**
 * This Interface must be used for validate credatials to login users.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @since 0.1
 * @version 1.0
 */
interface Authenticator {
	/**
	 * Takes the credentianls and validates if match with a valid user data.
	 * @return boolean whether the data match.
	 */
	public function authenticate($credentials): bool;
}