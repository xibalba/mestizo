<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\user;

use xibalba\mestizo\user\interfaces\BaseAbstract as IUser;

/**
 * Class BaseAbstract
 * This class provide an abstraction for implement a User Object.
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\mestizo\user
 */
abstract class BaseAbstract implements IUser {
	/**
	 * @var mixed id of the user. Null if the user is non authenticated.
	 */
	protected $_id = null;

	/**
	 * @var boolean true for an autheticated user.
	 */
	protected $_isLogged = false;

	public function __construct($id = null) {
		$this->_id = $id;
		if(!empty($id)) $this->_isLogged = true;
	}

	/**
	 * @return mixed user id value
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return boolean wherever the user is loged or not.
	 */
	public function isLogged(): bool {
		return $this->_isLogged;
	}

	public function login($id) : bool {
		$this->_id = $id;
		$this->_isLogged = true;
		return true;
	}

	public function logout(){
		$this->_id = null;
		$this->_isLogged = false;
	}
}
