<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/mestizo
 */

namespace xibalba\mestizo\user;
 
use xibalba\mestizo\user\interfaces\BaseAbstract as User;

/**
 * The Aware trait provide a property and accesor members for User object handling.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\mestizo\user
 */
trait Aware {
	/**
	 * @var User Some object that implement User interface.
	 */
	protected $_user = null;

	/**
	 * @return \xibalba\mestizo\user\interfaces\BaseAbstract Object.
	 */
	public function getUser() {
		if($this->_user instanceof User) return $this->_user;

		$userClass = $this->getConfig('user_class', $this->getConfig('ns').'\user\User');

		if(!class_exists($userClass))
			throw new \Exception('A Mestizo application require a «User» class that implements \xibalba\mestizo\user\interfaces\User interface.');

		$this->setUser(new $userClass());
		return $this->_user;
	}

	/**
	 * @param \xibalba\mestizo\user\interfaces\BaseAbstract $user Object that implement User interface.
	 */
	public function setUser(User $user) {
		$this->_user = $user;
	}
}