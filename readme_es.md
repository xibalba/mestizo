Mestizo, el framework ligero
===========================

Mestizo es un framework ligero con un ORM basado en el patrón "Mapa de datos" (Data Mapper ORM).

¿Otro framework? ¡Ya hay un montón!
---------------------------------

Bien... sí. pero este framework es diferente. :-)

En la actualidad existen dos tipos de frameworks:
1. Fullstack frameworks.
2. Microframeworks.

Podríamos agregar un tercer tipo: frameworks extenciones en C.

Los *fullstack* frameworks son herramientas poderosas, con cientos de funcionalidades, pero también son pesados, con curvas de aprendizaje algo empinadas, y cuyas funcionalidades más avanzadas son utilizadas solo en unos cuantos proyectos por unos cuantos desarrolladores.

Por otra parte, los microframeworks son muy livianos, pero muy del tipo Sinatra, con solo un conjunto específico de funcionalidades. Resultan útiles para el prototipado y aplicaciones ligeras, pero son limitados para proyectos mediados.

Mestizo comenzó como la idea de un ORM ligero para Yii2, pero la idea creció hasta convertirse en un framework MVC, tratando de ser poderoso y ligero, algo así como un punto medio entre los micro y los fullstack.