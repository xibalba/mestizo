<?php
/**
 * @copyright	2014-2015 Xibalba Lab.
 * @license     http://opensource.org/licenses/bsd-license.php
 * @link		https://bitbucket.org/xibalba/mestizo
 */

namespace michiq;

use xibalba\mestizo\Router;
use xibalba\mestizo\http\Request;

/**
 * A router resolve a url comparing the request url with the configured routes.
 * If some route match with the url, infers GET parameters and setup a request object.
 */
class RouterTest extends TestCase{
	
	private $__router;
	private $__request;
	
	private $__routes = [
		//Home rule:
		'/' => [
			'controller' => 'ControllerClass',
			'action' => 'actionMethodName'
		],
		
		//Static rule:
		'/static/content' => [
			'controller' => 'ControllerClass',
			'action' => 'actionMethodName'
		],
		
		//Combined rule:
		'/static/{action}/{slug}' => [
			'controller' => 'ControllerClass',
			'action' => ['key' => 'action',  'regex' => '(\w+)'],
			
			'params' => [
				['key' => 'slug',  'regex' => '(\w+)']
			]
		],
		
		// Namespaced rule
		// dynaNamespace/dynaController/dynaAction/dynaSlug
		'/{namespace}/{controller}/{action}/{slug}' => [
			'namespace' => ['key' => 'namespace',  'regex' => '(\w+)'],
			'controller' => [
				'key' => 'controller',
				'regex' => '(\w+)'
			],
			'action' => ['key' => 'action',  'regex' => '(\w+)'],
			
			'params' => [
				['key' => 'slug',  'regex' => '(\w+)']
			]
		],
		
		// Dynamic rule:
		// fictionController/fictionAction/paramX1/paramX2/paramX3
		'/{controller}/{action}/{param1}/{param2}/{param3}' => [
			'controller' => [
				'key' => 'controller',
				'regex' => '(\w+)'
			],
			'action' => [
				'key' => 'action',
				'regex' => '(\w+)'
			],
			'params' => [
				['key' => 'param1',  'regex' => '(\w+)'],
				['key' => 'param2',  'regex' => '(\w+)'],
				['key' => 'param3',  'regex' => '(\w+)']
			]
		]
	];
	
	public function setUp(){
		$this->__request = new Request();
		$this->__router = new Router($this->__routes);
		$this->__router->setRequest($this->__request);
	}
	
	public function testHome(){
		$rq = $this->__request;
		$router = $this->__router;
		
		$rq->server->set('REQUEST_URI', '/');
		$router->resolve();
		
		$expectedController = $this->__routes['/']['controller'];
		$expectedAction = $this->__routes['/']['action'];
		
		$this->assertEquals($expectedController, $router->getCurrentController());
		$this->assertEquals($expectedAction, $router->getCurrentAction());
	}
	
	public function testStaticRule(){
		$rq = $this->__request;
		$router = $this->__router;
		
		$rule = '/static/content';
		$rq->server->set('REQUEST_URI', $rule);
		
		$router->resolve();
		
		$expectedController = $this->__routes[$rule]['controller'];
		$expectedAction = $this->__routes[$rule]['action'];
		
		$this->assertEquals($expectedController, $router->getCurrentController());
		$this->assertEquals($expectedAction, $router->getCurrentAction());
	}
	
	public function testDynamicRule(){
		$rq = $this->__request;
		$router = $this->__router;
		
		$rq->server->set('REQUEST_URI', '/fictionController/fictionAction/paramX1/paramX2/paramX3');
		$router->resolve();
		
		$this->assertEquals('FictionController', $router->getCurrentController());
		$this->assertEquals('fictionAction', $router->getCurrentAction());
		
		//Test params
		$this->assertEquals('paramX1', $rq->get->get('param1'));
		$this->assertEquals('paramX2', $rq->get->get('param2'));
		$this->assertEquals('paramX3', $rq->get->get('param3'));
	}
	
	public function testCombinedRule(){
		$rq = $this->__request;
		$router = $this->__router;
		
		$rule = '/static/{action}/{slug}';
		
		$rq->server->set('REQUEST_URI', '/static/fictionAction/fictionSlug');
		$router->resolve();
		
		$expectedController = $this->__routes[$rule]['controller'];
		
		$this->assertEquals($expectedController, $router->getCurrentController());
		$this->assertEquals('fictionAction', $router->getCurrentAction());
		
		$this->assertEquals('fictionSlug', $rq->get->get('slug'));
	}
	
	public function testNamespaced() {
		$rq = $this->__request;
		$router = $this->__router;
		
		$rq->server->set('REQUEST_URI', '/dynaNamespace/dynaController/dynaAction/dynaSlug');
		$router->resolve();
		
		$this->assertEquals('dynaNamespace\DynaController', $router->getCurrentController());
		$this->assertEquals('dynaAction', $router->getCurrentAction());
		
		$this->assertEquals('dynaSlug', $rq->get->get('slug'));
	}
}
