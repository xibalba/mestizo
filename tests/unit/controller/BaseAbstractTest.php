<?php

namespace michiq\controller;

use michiq\TestCase;
use michiq\controller\stub\BaseAbstractStub;

use xibalba\mestizo\http\Request;

class BaseAbstractTest extends TestCase {
	
	/**
	 * Test that dispatching executes the actions of a controller
	 *
	 * @expectedException	xibalba\mestizo\http\Exception
	 */
	public function testDispatchAction(){
		$controllerStub = new BaseAbstractStub(new Request());
		$controllerStub->dispatchAction('riseException');
		$this->assertEquals('TestAction', $controllerStub->dispatchAction('action'));
	}
}
