<?php

namespace michiq\controller;

use michiq\TestCase;
use michiq\controller\stub\RestStub;

use xibalba\mestizo\http\Request;

class RestTest extends TestCase {
	
	/**
	 * Test that dispatching executes the actions of a controller
	 *
	 * @expectedException	xibalba\mestizo\http\Exception
	 */
	public function testDispatchAction(){
		$request = new Request();
		$request->setMethod('GET');
		
		$controllerStub = new RestStub($request);
		
		$controllerStub->dispatchAction('riseException');
		$this->assertEquals('TestGetAction', $controllerStub->dispatchAction('action'));
	}
}
