<?php

namespace michiq\controller\stub;

use xibalba\mestizo\controller\BaseAbstract;

class BaseAbstractStub extends BaseAbstract {
	public function action(){
		return 'TestAction';
	}
}