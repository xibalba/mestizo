<?php

namespace michiq\controller\stub;

use xibalba\mestizo\controller\Rest;

class RestStub extends Rest {
	public function getAction(){
		return 'TestGetAction';
	}
}