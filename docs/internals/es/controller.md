Controller
==========

*Ver: 0.1*
*Por: Yeshua Rodas <yrodas@upnfm.edu.hn> ☭*

Los controladores son las clases encargadas de procesar las peticiones realizadas a la aplicación. Cada petición es encausada hacia una *acción*. 

Acción
------
Una acción es un ***método público*** definido en las clases heredadas del controlador base, y es invocado cuando una petición hacia dicha acción es válida (por médio del despachador de acciones, método **`dispatchAction()`**).

Previo a la llamada a la acción deben realizace comprobaciones que determinen si la llamada puede o no realizarce. Para ello se configuran filtros que implementen la interfaz **`ActionFilter`**.

La interfáz **`ActionFilter`** espera que se implemente el método **`beforeAction`**, el cual siempre de llamará antes que la acción. En las controladores se puede sobreescribir para definir comprobaciones de último minuto.

La funcionalidad básica de filtros la brinda la clase **`AccessManager`** con tres filtros básicos:

1. Por verbos HTML.
2. Por IP.
3. Por rol de usuario (anónimo, registrado)

Además, el desarrollador puede crear sus propios filtros y agregarlos en el constructor del controlador, asegurándose que implementen la interfaz **`ActionFilter`**.

Las acciones pueden tener o no un prefijo y sufijo con fines de facilitar la lectura del código correspondiente, y así identificar que métodos del controlador se espera se llamen por el análisis de la peticiones y cuales son parte del cuerpo de  un controlador.

Renderizador
------------

Un renderizador es un *Trait* encargado de brindar la funcionalidad de respuesta a un controlador.
Este renderizado puede ser texto (como procesar una plantilla), devolver JSON, XML, o bien un archivo.

HtmlRenderer
------------

*Trait* que brinda la funcionalidad para renderizar html a partir de una plantilla php.