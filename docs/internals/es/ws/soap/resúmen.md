SOAP
----

Este componente es una funcionalidad extra sobre el framework dada la necesidad de conectar con otros servicios vía SOAP y XML (muy a pesar de que la industria se incline por REST y JSON).

Los clientes SOAP consumen recursos en base a apis:

    http://[dominio]/api/recurso
    
donde «recurso» puede resolverse para entregar un WSDL que apunte a la misma url, u a otra, para ser consumido.

La resolución se hace en base a un *controlador*, este debe implementar dos elemenos:

* El generador de WSDL
* El instanciador del servidor SOAP.

Un «recurso» brindará una funcionalidad limitada, construída a partir de una clases análoga a un controlador que brinda los métodos que pueden ser consumidos por los clientes para cada recurso. El WSDL para cada recurso es construído a partir de dicha clase con métodos que sean **públicos** y que posean en su documentación la etiqueta **`@soap`**.


