Mestizo, El framework ligero
============================

*Ver: 0.2*
*Por: Yeshua Rodas <yrodas@upnfm.edu.hn> ☭*

Contexto
-------

Existen un gran número de frameworks PHP para el desarrollo de software, la mayoría de código abierto, cada cual más o menos popular que los otros.

Hay buenas razones por la cual PHP y ciertos frameworks en particular son muy populares y utilizados en el desarrollo del software, citaré algunos puntos, cada cual con sus ventajas y desventajas, para sustentar el desarrollo de uno más:

* **PHP es un lenguaje sencillo.** Y eso significa que un programador novel pronto puede estar dando resultados. Pero esto también implica un riezgo: requiere una mayor autodisciplina, ya que el lenguaje permite tanta libertad en el desarrollo que puede volverse sencillo el caer en malas prácticas y terminar con malos espaguetis. Los frameworks, además de facilitar el desarrollo con sus bibliotecas, también brindan un marco ordenado para mantener el control sobre el desarrollo.

* **Los frameworks ya han solucionado problemas.** Una de las grandes ventajas de los frameworks es la comunidad que le respalda. Estas comunidades son heterogéneas, con gurúes brillantes y noveles en pininos. Esto propicia el crecimiento del conocimiento colectivo, y eso beneficia los proyectos, los noveles saben donde acudir y los gurúes encuentran retos que resolver.

* **Los frameworks con el orden, agregan peso y complejidad.** El orden no puede ser gratis. El orden exige comprobaciones, estructura, disciplina, y esto pasa factura a los recursos. El reto es mantener un equilibrio entre suficiente orden y eficiencia de ejecución.

Se han popularizado dos tipos de frameworks:

1. Los llamados "fullstack", que son frameworks grandes, con una arquitectura muy bien definida, y con muchas funcionalidades. Estos frameworks son respandados por grandes comunidades y a veces por empresas. Los más conocidos son Symfony, Yii, Codeigniter, Laravel.

2. Los "microframeworks", que tienen una arquitectura más "suave" y una cantidad de funcionalidades limitadas. Son muy populares para el prototipado, proyectos pequeños,  desarrollos muy personalizados y desarrollo de API.

Siempre se busca el mejor aprovechamiento de los recursos, la arquitectura dinámica de PHP, su facilidad, que se traduce en un desarrollo más rápido de aplicaciones, también significa un consumo mayor de recursos. En la búsqueda de obtener las ventajas de los frameworks y reducir el costo en recursos que ello implica se ha desarrollado un tercer tipo de framework *fullstack*: **las extenciones de C**. Conozco dos: YAF y Phalcon.

Las grandes aplicaciones se ven muy bien beneficiadas por los grandes frameworks, las pequeñas aplicaciones se nutren muy bien de los microframeworks. ¿Y la media? En mi opinión, para aprovechar de mejor manera los recursos, es necesario un framework extensible diseñado desde un principio para aplicaciones medias.

Requerimientos
-------

Los siguientes requerimientos son básicos para la primer versión del framework:

1. Debe ser soportado por PHP 5.4 mínimo.
2. Debe ser compaible con HHVM.
3. Cada elemento del patrón MVC debe ser independiente del resto.
4. Compatible con composer.
5. Soporte para Web Services (SOAP y REST).


General
-------

El framework debe cumplir con los siguientes componentes:

* Un ORM independiente (ver [Alpaca](https://bitbucket.org/xibalba/alpaca)).
* Un Controlador base.
* Un renderizador de vistas basado en PHP, sin motor de plantillas.
* Un enrutador.
* Una aplicación despachadora.
* Un gestor de Excepciones.
* Un paquete de utilidades (ver [Ocelote](https://bitbucket.org/xibalba/ocelote)).

El ORM
------

[Consultar Alpaca ORM](https://bitbucket.org/xibalba/alpaca)

El Controlador
--------------

Esta parte es la encargada de procesar las peticiones, realizar un proceso, y preparar una salida. Dicho proceso puede incluir consultas al Modelo, normalemte por medio del ORM (aunque no siempre es así), preparar una respuesta, y enviarla.
El controlador espera a cualquier tipo de petición: Http, AJAX, SOAP, REST, etc.

Cuando la petición no es Http, la respuesta normalmente serán datos en formato JSON o XML.

El Renderizador
---------------

Hay diversos tipos de vistas, dependiendo de la arquitectura de la aplicación. Cuando se responde con html, la vista no es más que un archivo php que procesa los datos preparados por el controlador y les da "forma". El renderizador debe ser una interfaz que utilice el controlador para devolver este tipo de vistas.

El Enrutador
------------

Las peticiones siguen el siguiente ciclo:

1. El navegador envía una petición (http o https)
2. La petición es capturada por un único punto de entrada (index.php) que crea un objeto aplicación.
3. La aplicación crea un objeto enrutador, que toma la URI y la analiza.
4. El enrutador determina el controllador y acción a llamar, establece los parámetros GET para un objeto petición y devuelve el control a la app, o bien, lanza una Excepción.
5. La aplicación llama al controlador.
6. El controlador realiza su trabajo. :)

La misión del enrutador es analizar la URI para determinar si la petición procede o no.

La Aplicación despachadora
--------------------------

Se encarga de encausar la petición al enrutador, llamar al controlador correspondiente y capturar o lanzar excepciones.

La gestión de excepciones
-------------------------

La ejecución de todo software puede dar lugar a excepciones. Algunas son comunes a todo proyecto, como las excepciones Http, este componente debe contener todas las excepciones básicas necesarias.

Paquete de utilidades de cadenas y arrays
------------------------------

[Consultar Ocelote](https://bitbucket.org/xibalba/ocelote)