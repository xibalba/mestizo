View
==========

*Ver: 0.1*
*Por: Yeshua Rodas <yrodas@upnfm.edu.hn> ☭*

La vista es la parte encargada de presentar la información al usuario. Es este componente el encargado de poner formato y estilo a la presentación que será mostrada.

Lo típico es una conjunción de plantillas procesadas de manera jerárquica, desde lo más profundo hasta el lienzo global.

La vista básica es provista por la clase **`HtmlRenderer`**.

No obstante, no todas la vistas son de este tipo.

Una aplicación puede servir simplemente como API, y los datos son devueltos como texto ***`JSON`*** o ***`XML`***. En estos casos la vista se limita a dar formato al texto a devolver y la aplicación enviará la respuesta al cliente.

HtmlRenderer
------------

Las vistas se componen de tres elementos:

* Layouts (Envoltorios)
* Templates (Plantillas)
* Partials (parciales)

### Layouts

Un Layout es un envoltorio general para las plantillas. Se trata de estilos y html que siempre se muestra, indepentientemente de la plantilla a mostrar. Por lo genral contendrá la cabecera (header) y el pie de página (footer) de la web.
Cada controlador define el layout a utilizar. Si el usuario lo requiere, el layout puede cambiarse.

### Templates

Una plantilla es una vista específica, muestra datos concretos, por ejemplo las entradas de un blog, un listado de artículos, un bloque de comentarios, etc.
Por lo general cada acción del controlador renderizará su propia plantilla (no necesariamente en todos los casos).

### Partials

Un parcial es un trozo de plantilla reutilizable, son elementos que se repiten en ciertas plantillas, pero no en todas, por ejemplo, un bloque de comentario, un tipo de menú, una baraa lateral, etc. Los parciales siempre son devueltos, nunca renderizados.



`HtmlRenderer` toma un 