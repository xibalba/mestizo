Application
===========

*Ver: 0.1*
*Por: Yeshua Rodas <yrodas@upnfm.edu.hn> ☭*

Una *Aplicación* MVC se estructura de los siguientes componentes:

* Modelos
* Controladores
* Vistas
* Ayudantes
* Utilidades
* Despachador

La clase abstracta `Application` es la clase base para toda aplicación. Sus tres funciones son:

* Capturar la configuración de la aplicación:
    * Nombre de la applicación (opcional)
    * Namespace de la aplicación (requerido)
    * Rutas (requerido)
    * Parámetros globales (opcional)
    * Configuración de la base de datos (opcional)   
* Capturar la petición http genérica y despacharla.
* Brindar métodos de acceso a los componentes globales:
    * Sesión.
    * Usuario.
    * Petición (Request)

Flujo
-----

El fujo de una petición es:
App → Controlador → Acción → Vista → Respuesta.

La clase `Application` es un controlador frontal que funciona como único punto de entrada a la aplicación.

El usuario debe crear su propia clase App que extienda de `Application`.
Cada petición contruye un objeto anónimo de la aplicación.
Al momento de la construcción se valida la configuración de la aplicación y se cargan sus componentes.

Al ejecutar (llamada al método `run()`) se crea un objeto *enrutador* que determina los parámetros get (para mantener url amigables), el controlador y acción a  invocar, y un objero *Request* (Petición) que encapsula la petición per se.

Si no se lanza una excepción, entonces se pasa el control al controlador.
