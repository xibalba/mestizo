Mestizo Light Framework
======================

Mestizo is a light framework with a Data Mapper ORM.

Another framework? Out there are a lot!
---------------------------------------

Well... Yes. But this framework is diferent. :-)

This days existi tow kind of frameworks:
1. Fullstack frameworks.
2. Microframeworks.

Well, we can add a third type: C extension frameworks.

The fullstack frameworks are powerful tools, but are heavy too, with hundred of functionalities that are used just by a few developers.

By other hand, the microframeworks are very light, but are a Sinatra way too, with a lot of missed functionalities.

Mestizo start like a ORM for Yii2, but now becomes a light MVC framework, trying to be powerful and light, some like a middle point between micro and fullstack.